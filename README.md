Sandsail (svoboj48)
====
* Second HLAA NOTA assignment solution.
* Only the BASE variant
* `armbcom` is moving in the wind direction, always the same distance
* Units that are not `armbcom` do not move when assigned to the behaviour instance

Trees
----
* FollowWindDirection

Sensors
----
* WindDirection

Resources
----
* FollowWindDirection tree command icon: 
    * made by [Yannick](https://www.flaticon.com/authors/yannick) from [www.flaticon.com](https://www.flaticon.com)
    * distributed with the [CC BY 3.0](https://creativecommons.org/licenses/by/3.0/) licence 
